FROM ubuntu:20.04

MAINTAINER Alessio Piucci <alessio.piucci@cern.ch>

USER root
RUN apt-get -y update

##########
## install the basic libraries
##########

# install basic libraries
RUN apt-get -y install build-essential libx11-dev libxpm-dev libxft-dev libxext-dev libpcre3-dev\
    gfortran libssl-dev libgl1-mesa-dev libglew1.5-dev libftgl-dev libmysqlclient-dev libfftw3-dev\
    libgraphviz-dev libavahi-compat-libdnssd-dev libldap2-dev libxml2-dev libafterimage0 libafterimage-dev\
    graphviz exiv2
    
# some mpre useful software
RUN apt-get -y install cmake emacs git zsh libcfitsio-dev

# install Boost
RUN apt-get -y update \
    && apt-get -y install libboost-all-dev

# gsl and clang
RUN apt-get -y update \
    && apt-get -y install libgslcblas0 libgsl-dev clang

# install pip for Python 3
RUN apt-get install python3-pip

# install Numpy for Python 3
RUN pip3 install numpy

##########
## install xrootd and ROOT
##########

# setup the work directory
WORKDIR /tmp

# install Kerberos, disabling the interactive input
RUN apt-get -y update \
    && DEBIAN_FRONTEND=noninteractive apt-get -y install libkrb5-dev krb5-config krb5-doc

# install xrootd v4.11.0
# v5.0.0 causes an error when compiling Root
RUN cd /tmp; git clone --depth 1 http://github.com/xrootd/xrootd.git -b stable-4.11.x --single-branch \
    && mkdir xrootd-build \
    && cd xrootd-build \
    && cmake ../xrootd -DCMAKE_INSTALL_PREFIX=/usr/local -DENABLE_PERL=FALSE -DENABLE_FUSE=FALSE -DENABLE_KRB5=TRUE -DCMAKE_CXX_FLAGS="-Wterminate" \
    && make -j 8 && make install \
    && cd .. && rm -rf xrootd xrootd-build

# install TBB
RUN apt-get -y install libtbb-dev

# install Root with roofit, mathmore and minuit2
RUN git clone --depth 1 http://root.cern.ch/git/root.git -b v6-18-04 --single-branch \
    && mkdir root-build \
    && cd root-build \
    && cmake ../root -Dmathmore=ON -Dminuit2=ON -Droofit=ON -Dx11=ON -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_INSTALL_PREFIX=/usr/local/root -Dxrootd=ON -DCMAKE_CXX_STANDARD=17 \
    && make -j 8 \
    && cmake --build . --target install \
    && cd .. \
    && rm -rf root root-build

# disable the interactive GUI, and link to the Root installation
RUN cd ; echo 'PATH=$PATH:"/usr/local/root/bin"; export DISPLAY=localhost:0.0' >> .bashrc \\
    && echo 'source /usr/local/root/bin/thisroot.sh' >> .bashrc
